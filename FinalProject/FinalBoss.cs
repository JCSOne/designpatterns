﻿using System;

namespace FinalProject
{
    public class FinalBoss : Enemy
    {
        public override Enemy Clone()
        {
            return (Enemy) MemberwiseClone();
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a final boss");
        }

        public override void Update()
        {
            Console.WriteLine("Updating a final boss");
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace FinalProject
{
    public abstract class Hero : ICloneable
    {
        private readonly AttributesList _attributesList = new AttributesList();
        public int X { get; set; }
        public int Y { get; set; }
        public double Direction { get; set; }
        private List<Enemy> _enemies { get; } = new List<Enemy>();
        public IIterable<Attribute> Attributes => _attributesList.GetIterator();

        public object Clone()
        {
            return MemberwiseClone();
        }

        public void Attach(Enemy enemy)
        {
            if (!_enemies.Contains(enemy)) _enemies.Add(enemy);
        }

        public void Detach(Enemy enemy)
        {
            if (_enemies.Contains(enemy)) _enemies.Remove(enemy);
        }

        public void Draw()
        {
            Console.WriteLine($"Drawing the hero in the ({X}, {Y}) coordinates");
        }

        public void Move(int units)
        {
            if (units == 0)
            {
                Console.WriteLine("The hero is in the same place");
                return;
            }

            X = X + (int) (units * Math.Cos(Direction));
            Y = Y + (int) (units * Math.Sin(Direction));
            Console.WriteLine($"The hero has advanced {units} units and is now in the ({X}, {Y}) coordinates");
            NotifyObservers();
        }

        private void NotifyObservers()
        {
            foreach (var enemy in _enemies) enemy.UpdateHeroPosition(this);
        }

        public void Rotate(double direction)
        {
            Console.WriteLine($"The hero has rotated {direction} degrees to the {(direction > 0 ? "left" : "right")}");
            Direction = (Direction + direction) % 360;
        }

        public void Update()
        {
            Console.WriteLine("Updating the hero");
        }
    }
}
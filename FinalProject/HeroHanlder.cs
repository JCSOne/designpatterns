﻿namespace FinalProject
{
    public abstract class HeroHandler
    {
        protected HeroHandler(Hero hero)
        {
            Hero = hero;
        }

        public Hero Hero { get; }

        public void Attach(Enemy enemy)
        {
            Hero.Attach(enemy);
        }


        public void Detach(Enemy enemy)
        {
            Hero.Detach(enemy);
        }

        public void Draw()
        {
            Hero.Draw();
        }

        public void Move(int units)
        {
            Hero.Move(units);
        }

        public void Rotate(double direction)
        {
            Hero.Rotate(direction);
        }

        public void Update()
        {
            Hero.Update();
        }

        public abstract void GetHealth();
    }
}
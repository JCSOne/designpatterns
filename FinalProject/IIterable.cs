﻿namespace FinalProject
{
    public interface IIterable<T>
    {
        void Add(T value);
        T Next();
        bool HasNext();
    }
}
﻿using System;

namespace FinalProject
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Bridge pattern
            HeroHandler walkingHeroHandler = new WalkingHeroHandler(new WalkingHero());
            HeroHandler flyingHeroHandler = new FlyingHeroHandler(new FlyingHero());

            Console.WriteLine("Walking hero");
            walkingHeroHandler.GetHealth();

            Console.WriteLine();

            Console.WriteLine("Flying Hero");
            flyingHeroHandler.GetHealth();

            Console.WriteLine();

            var hero = walkingHeroHandler.Hero;

            // Composite
            var intelligence = new Attribute("Inteligencia", 10, 1);

            var cunning = new Attribute("Astucia", 10, 2);
            cunning.Upgrade(new Attribute("Astucia nivel 2", 20, 2));
            cunning.Upgrade(new Attribute("Astucia nivel 3", 20, 2));
            cunning.Upgrade(new Attribute("Astucia nivel 4", 20, 2));

            var problemSolving = new Attribute("Solucion de problemas", 20, 3);
            problemSolving.Upgrade(new Attribute("Solucion de problemas nivel 1", 40, 2));

            intelligence.Upgrade(cunning);
            intelligence.Upgrade(problemSolving);

            // Iterator
            hero.Attributes.Add(intelligence);
            hero.Attributes.Add(cunning);
            hero.Attributes.Add(problemSolving);

            var attributes = hero.Attributes;

            while (attributes.HasNext())
            {
                var attribute = attributes.Next();
                Console.WriteLine(attribute.Name);
                foreach (var addOn in attribute.AddOns) Console.WriteLine(addOn.Name);
            }

            Console.WriteLine();

            // Prototype & Singleton
            var minion = EnemiesCache.Instance.GetEnemy("minion");
            minion.Update();
            minion.Draw();

            var miniBoss = EnemiesCache.Instance.GetEnemy("miniBoss");
            miniBoss.Update();
            miniBoss.Draw();

            var finalBoss = EnemiesCache.Instance.GetEnemy("finalBoss");
            finalBoss.Update();
            finalBoss.Draw();

            Console.WriteLine();

            // Observer
            hero.Attach(minion);
            hero.Attach(miniBoss);
            hero.Attach(finalBoss);

            // Command
            var controller = new HeroController();

            controller.SetCommand(new MoveCommand(hero) {Distance = 100});
            controller.ExecuteCommands();

            // Memento
            Console.WriteLine();
            Console.WriteLine("Saving the game");
            var gameSave = new GameSave {GameState = new GameState {Hero = hero, Score = 1000}};

            controller.SetCommand(new RotateCommand(hero) {Direction = 45});
            controller.SetCommand(new MoveCommand(hero) {Distance = 10});
            controller.ExecuteCommands();

            Console.WriteLine();
            Console.WriteLine("Loading the game");
            hero = gameSave.GameState.Hero;
            var score = gameSave.GameState.Score;
            Console.WriteLine(
                $"Game loaded, hero went back to the ({gameSave.GameState.Hero.X}, {gameSave.GameState.Hero.Y}) coordinates, with a score of {score}");

            Console.ReadLine();
        }
    }
}
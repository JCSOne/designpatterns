﻿using System;

namespace FinalProject
{
    internal class WalkingHeroHandler : HeroHandler
    {
        public WalkingHeroHandler(Hero hero) : base(hero)
        {
        }

        public override void GetHealth()
        {
            Console.WriteLine("The hero can still walk");
        }
    }
}
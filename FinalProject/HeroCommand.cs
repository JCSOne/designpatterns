﻿namespace FinalProject
{
    public abstract class HeroCommand
    {
        public HeroCommand(Hero hero)
        {
            Hero = hero;
        }

        protected Hero Hero { get; }

        public abstract void Execute();
    }
}
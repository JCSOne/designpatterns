﻿namespace FinalProject
{
    public class MoveCommand : HeroCommand
    {
        public MoveCommand(Hero hero) : base(hero)
        {
        }

        public int Distance { get; set; }

        public override void Execute()
        {
            Hero.Move(Distance);
        }
    }
}
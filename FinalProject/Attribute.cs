﻿using System.Collections.Generic;

namespace FinalProject
{
    public class Attribute
    {
        public Attribute(string name, int value, int multiplier)
        {
            AddOns = new List<Attribute>();
            Name = name;
            BaseValue = value;
            Multiplier = multiplier;
        }

        public List<Attribute> AddOns { get; }

        public string Name { get; }
        public int BaseValue { get; }
        public int Multiplier { get; }

        public void Upgrade(Attribute addOn)
        {
            AddOns.Add(addOn);
        }

        public void Downgrade(Attribute addOn)
        {
            AddOns.Remove(addOn);
        }
    }
}
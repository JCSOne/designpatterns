﻿using System;

namespace FinalProject
{
    public abstract class Enemy
    {
        public abstract Enemy Clone();
        public abstract void Draw();
        public abstract void Update();

        public void UpdateHeroPosition(Hero hero)
        {
            Console.WriteLine(
                $"The enemy is now moving in direction to the ({hero.X}, {hero.Y}) coordinates where the hero is");
        }
    }
}
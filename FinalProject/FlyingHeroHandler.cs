﻿using System;

namespace FinalProject
{
    internal class FlyingHeroHandler : HeroHandler
    {
        public FlyingHeroHandler(Hero hero) : base(hero)
        {
        }

        public override void GetHealth()
        {
            Console.WriteLine("The hero can still fly");
        }
    }
}
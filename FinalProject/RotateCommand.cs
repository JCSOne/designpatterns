﻿namespace FinalProject
{
    public class RotateCommand : HeroCommand
    {
        public RotateCommand(Hero hero) : base(hero)
        {
        }

        public double Direction { get; set; }

        public override void Execute()
        {
            Hero.Rotate(Direction);
        }
    }
}
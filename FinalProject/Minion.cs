﻿using System;

namespace FinalProject
{
    public class Minion : Enemy
    {
        public override Enemy Clone()
        {
            return (Enemy) MemberwiseClone();
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a minion");
        }

        public override void Update()
        {
            Console.WriteLine("Updating a minion");
        }
    }
}
﻿using System.Collections.Generic;

namespace FinalProject
{
    public class AttributesList
    {
        protected readonly List<Attribute> _attributes;

        public AttributesList()
        {
            _attributes = new List<Attribute>();
        }

        public IIterable<Attribute> GetIterator()
        {
            return new AttributesIterator(_attributes);
        }

        private class AttributesIterator : IIterable<Attribute>
        {
            private readonly List<Attribute> _attributes;
            private int _index;

            public AttributesIterator(List<Attribute> attributes)
            {
                _index = 0;
                _attributes = attributes;
            }

            public void Add(Attribute value)
            {
                _attributes.Add(value);
            }

            public Attribute Next()
            {
                var attribute = _attributes[_index];
                _index++;
                return attribute;
            }

            public bool HasNext()
            {
                return _index < _attributes.Count;
            }
        }
    }
}
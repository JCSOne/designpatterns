﻿using System;

namespace FinalProject
{
    public class MiniBoss : Enemy
    {
        public override Enemy Clone()
        {
            return (Enemy) MemberwiseClone();
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a mini boss");
        }

        public override void Update()
        {
            Console.WriteLine("Updating a mini boss");
        }
    }
}
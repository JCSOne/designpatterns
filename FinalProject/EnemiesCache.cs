﻿using System.Collections.Generic;

namespace FinalProject
{
    public class EnemiesCache
    {
        private static Dictionary<string, Enemy> _enemiesCache;

        private static EnemiesCache _instance;

        private EnemiesCache()
        {
            _enemiesCache = new Dictionary<string, Enemy>
            {
                {"minion", new Minion()},
                {"miniBoss", new MiniBoss()},
                {"finalBoss", new FinalBoss()}
            };
        }

        public static EnemiesCache Instance => _instance ?? (_instance = new EnemiesCache());

        public Enemy GetEnemy(string type)
        {
            return _enemiesCache.GetValueOrDefault(type).Clone();
        }
    }
}
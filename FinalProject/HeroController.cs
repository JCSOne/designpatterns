﻿using System;
using System.Collections.Generic;

namespace FinalProject
{
    public class HeroController
    {
        private readonly Queue<HeroCommand> _commands = new Queue<HeroCommand>();

        public void SetCommand(HeroCommand command)
        {
            _commands.Enqueue(command);
        }

        public void ExecuteCommands()
        {
            while (_commands.TryDequeue(out var command))
            {
                Console.WriteLine();
                command.Execute();
            }
        }
    }
}
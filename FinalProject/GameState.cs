﻿namespace FinalProject
{
    public class GameState
    {
        private Hero _hero;

        public Hero Hero
        {
            get => _hero;
            set => _hero = (Hero) value.Clone();
        }

        public int Score { get; set; }
    }
}